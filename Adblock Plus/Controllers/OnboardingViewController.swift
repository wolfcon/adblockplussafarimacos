/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import Markup
import SafariServices

class OnboardingViewController: NSViewController {

    private var contentBlockerIsEnabled = false

    //Markup Renderer
    private let renderer = MarkupRenderer(baseFont: .systemFont(ofSize: 16))

    //Outlets for first screen
    @IBOutlet weak var screenOne: NSView!
    @IBOutlet weak var launchPreferencesButton: NSButton!
    @IBOutlet weak var screenOneHeaderLabel: NSTextField!
    @IBOutlet weak var screenOneInstructionsLabel: NSTextField!
    @IBOutlet weak var screenTwoHeaderLabel: NSTextField!

    //Outlets for second screen
    @IBOutlet weak var screenTwo: NSView!
    @IBOutlet weak var firstContentFrame: NSView!
    @IBOutlet weak var secondContentFrame: NSView!
    @IBOutlet weak var finishButton: NSButton!

    override func viewWillAppear() {
        // Removes title bar from window and prevents from being minimised/closed 
        self.view.window?.titleVisibility = .hidden
        self.view.window?.titlebarAppearsTransparent = true
        self.view.window?.styleMask.insert(.fullSizeContentView)
        self.view.window?.styleMask.remove(.closable)
        self.view.window?.styleMask.remove(.fullScreen)
        self.view.window?.styleMask.remove(.miniaturizable)
        self.view.window?.styleMask.remove(.resizable)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { state, _ in
            self.contentBlockerIsEnabled = state?.isEnabled ?? false
            self.setUpLayers()
            self.applyAttributedStrings()
        }
    }

    func setUpLayers() {
        self.firstContentFrame.applyBorderStyle()
        self.secondContentFrame.applyBorderStyle()
    }

    func applyAttributedStrings() {
        let screenOneHeaderString = "You’re *one step away* from surfing the web without annoying ads!".localized
        screenOneHeaderLabel.attributedStringValue = renderer.render(text: screenOneHeaderString)

        let screenTwoHeaderString = "*All done!* Adblock Plus is now ready to block ads.".localized
        screenTwoHeaderLabel.attributedStringValue = renderer.render(text: screenTwoHeaderString)

        switch contentBlockerIsEnabled {
        case true:
            let userInstructionsString = "Turn on *ABP Control Panel* in Safari Extension Preferences to enable it in your Safari toolbar.".localized
            screenOneInstructionsLabel.attributedStringValue = renderer.render(text: userInstructionsString)
        case false:
            let userInstructionsString = "Please *turn on Adblock Plus* in Safari Extension Preferences.".localized
            screenOneInstructionsLabel.attributedStringValue = renderer.render(text: userInstructionsString)
        }
    }

    @IBAction func didPressLaunchPreferencesButton(_ sender: Any) {
        SFSafariApplication.showPreferencesForExtension(withIdentifier: Constants.contentBlockerIdentifier) { _ in
            //open safari preferences for extensions
        }
        self.showSecondScreen()
    }

    func showSecondScreen() {
            self.screenOne.isHidden = true
            self.screenTwo.isHidden = false
    }

    @IBAction func didPressFinishButton(_ sender: Any) {
        dismiss(self)
    }
}

// Extends NSView to abstract application of CALayer styles.
extension NSView {

    /// Applies CALayer with grey border around caller.
    func applyBorderStyle() {
        let layer = CALayer()
        layer.cornerRadius = 4.0
        layer.borderWidth = 1.0
        layer.borderColor = NSColor.customColor(.onboardingBorder).cgColor
        layer.masksToBounds = false
        layer.needsDisplayOnBoundsChange = true
        self.wantsLayer = true
        self.layer = layer
    }
}
