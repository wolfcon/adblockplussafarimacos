/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

enum FilterlistLanguage: String {
    case english = "en"
    case german = "de"
    case spanish = "es"
    case french = "fr"
    case italian = "it"
    case dutch = "nl"
    case russian = "ru"
    case chinese = "zh"
}

struct Filterlist {
    let title: String
    let description: String
    let files: [String]
    let language: FilterlistLanguage
    let tag: Int
}

struct Filterlists {
    static let filterlists = [
        Filterlist(title: "English",
                   description: "(Easylist)",
                   files: ["easylist_content_blocker",
                           "easylist+exceptionrules_content_blocker"],
                   language: .english,
                   tag: 0),
        Filterlist(title: "Deutsch + English",
                   description: "(EasyList Germany+EasyList)",
                   files: ["easylist+easylistgermany-minified",
                           "easylist+easylistgermany-minified+exceptionrules-minimal"],
                   language: .german,
                   tag: 1),
        Filterlist(title: "español + English",
                   description: "(EasyList Spanish+EasyList)",
                   files: ["easylist+easylistspanish-minified",
                           "easylist+easylistspanish-minified+exceptionrules-minimal"],
                   language: .spanish,
                   tag: 2),
        Filterlist(title: "français + English",
                   description: "(Liste FR+EasyList)",
                   files: ["easylist+liste_fr-minified",
                           "easylist+liste_fr-minified+exceptionrules-minimal"],
                   language: .french,
                   tag: 3),
        Filterlist(title: "italiano + English",
                   description: "(EasyList Italy+EasyList)",
                   files: ["easylist+easylistitaly-minified",
                           "easylist+easylistitaly-minified+exceptionrules-minimal"],
                   language: .italian,
                   tag: 4),
        Filterlist(title: "Nederlands + English",
                   description: "(EasyList Dutch+EasyList)",
                   files: ["easylist+easylistdutch-minified",
                           "easylist+easylistdutch-minified+exceptionrules-minimal"],
                   language: .dutch,
                   tag: 5),
        Filterlist(title: "русский, українська + English",
                   description: "(RuAdList+EasyList)",
                   files: ["easylist+ruadlist-minified",
                           "easylist+ruadlist-minified+exceptionrules-minimal"],
                   language: .russian,
                   tag: 6),
        Filterlist(title: "中文 + English",
                   description: "(EasyList China+EasyList)",
                   files: ["easylist+easylistchina-minified", "easylist+easylistchina-minified+exceptionrules-minimal"],
                   language: .chinese,
                   tag: 7)
    ]
}
