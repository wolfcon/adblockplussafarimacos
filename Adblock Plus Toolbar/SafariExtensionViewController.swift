/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class SafariExtensionViewController: SFSafariExtensionViewController {

    static let shared = SafariExtensionViewController()
    private var url: String?
    private var urlHostname: String?
    private let dataSource = WhitelistItemDataSource()
    private let notification = Notification.Name(rawValue: "\(Constants.safariToolbarIdentifier).whitelist")
    @IBOutlet weak var domainNameLabel: NSTextField!
    @IBOutlet weak var whitelistCheckboxOutlet: NSButton!
    @IBOutlet weak var whitelistSpinner: NSProgressIndicator!

    @IBAction func openHostAppAction(_ sender: NSButton) {
        NSWorkspace.shared.launchApplication("Adblock Plus")
    }

    @IBAction func whitelistCheckboxAction(_ sender: NSButton) {
        guard let whitelistHostname = self.urlHostname else {
            return
        }

        whitelistCheckboxOutlet.isEnabled = false
        whitelistSpinner.startAnimation(nil)
        let whitelistArray = dataSource.getWhitelistArray()

        switch whitelistArray.contains(whitelistHostname) {
        case true:
            // Remove URL from whitelist.
            dataSource.removeURLfromArray(url: whitelistHostname)

            // Apply Content Blocker Rules
            let whitelistManager = WhitelistManager()
            let modifiedWhitelistArray = dataSource.getWhitelistArray()
            whitelistManager.applyWhitelist(modifiedWhitelistArray)
            // Reload Content Blocker
            SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { _ in
                // Update Outlets
                DispatchQueue.main.async {
                    self.whitelistCheckboxOutlet.isEnabled = true
                    self.whitelistCheckboxOutlet.title = "Enabled on this site".localized
                    self.whitelistCheckboxOutlet.state = .on
                    self.whitelistSpinner.stopAnimation(nil)
                }
                // Send Update To Host App
                DistributedNotificationCenter.default().post(name: self.notification, object: Constants.safariToolbarIdentifier)
                // Reload Page
                self.reloadCurrentPage()
            }
        case false:
            // Add URL to whitelist
            dataSource.appendToWhitelist(hostname: whitelistHostname)

            // Apply Content Blocker Rules
            let whitelistManager = WhitelistManager()
            let modifiedWhitelistArray = dataSource.getWhitelistArray()
            whitelistManager.applyWhitelist(modifiedWhitelistArray)

            // Reload Content Blocker
            SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { _ in
                // Update Outlets
                DispatchQueue.main.async {
                    self.whitelistCheckboxOutlet.isEnabled = true
                    self.whitelistCheckboxOutlet.title = "Disabled on this site".localized
                    self.whitelistCheckboxOutlet.state = .off
                    self.whitelistSpinner.stopAnimation(nil)
                }
                // Send Update To Host App
                DistributedNotificationCenter.default().post(name: self.notification, object: Constants.safariToolbarIdentifier)
                // Reload Page
                self.reloadCurrentPage()
            }
        }
    }

    private func getActiveURL() {
        SFSafariApplication.getActiveWindow(completionHandler: { window in
            window?.getActiveTab(completionHandler: { activeTab in
                activeTab?.getActivePage(completionHandler: { activePage in
                    activePage?.getPropertiesWithCompletionHandler({ properties in
                        self.url = properties?.url?.absoluteString ?? ""
                    })
                })
            })
        })
    }

    private func reloadCurrentPage() {
        SFSafariApplication.getActiveWindow(completionHandler: { window in
            window?.getActiveTab(completionHandler: { tab in
                tab?.getActivePage(completionHandler: { page in
                    page?.reload()
                })
            })
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = NSSize(width: self.view.frame.size.width, height: self.view.frame.size.height)

        getActiveURL()
    }

    func onPopoverVisible(with url: String?) {
        self.url = url
        configureOutlets()
    }

    func configureOutlets() {
        let whitelistArray = self.dataSource.getWhitelistArray()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.33) {
            self.domainNameLabel.isHidden = false
            self.whitelistCheckboxOutlet.isHidden = false
            guard let url = self.url else {
                self.domainNameLabel.stringValue = "No URL found".localized
                self.whitelistCheckboxOutlet.isEnabled = false
                self.whitelistCheckboxOutlet.state = .mixed
                return
            }

            guard let urlHostname = NSString(string: url).whitelistedHostname() else {
                self.domainNameLabel.stringValue = "No URL found".localized
                self.whitelistCheckboxOutlet.isEnabled = false
                self.whitelistCheckboxOutlet.state = .mixed
                return
            }
            self.domainNameLabel.stringValue = urlHostname
            self.whitelistCheckboxOutlet.isEnabled = true
            self.urlHostname = urlHostname
            switch whitelistArray.contains(urlHostname) {
            case true:
                self.whitelistCheckboxOutlet.title = "Disabled on this site".localized
                self.whitelistCheckboxOutlet.state = .off
            case false:
                self.whitelistCheckboxOutlet.title = "Enabled on this site".localized
                self.whitelistCheckboxOutlet.state = .on
            }
        }
    }
}
